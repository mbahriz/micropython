# CODE : capa0.py
import machine

PIN_gene = machine.Pin(15, machine.Pin.OUT)
PIN_lire = machine.ADC(28)

PIN_gene.value(1)
conversion = 3.3/65535
while True:
    tension = PIN_lire.read_u16()*conversion
    print("Uc = {:.3f} V".format(tension))