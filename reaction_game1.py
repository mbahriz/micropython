# CODE : reaction_game1.py
import machine 
import utime 
import urandom

led = machine.Pin(15, machine.Pin.OUT) 
bouton = machine.Pin(16, machine.Pin.IN, machine.Pin.PULL_DOWN)

def action_bouton(pin):
    bouton.irq(handler=None) # l actionnement du bouton n appellera plus de fonction
    temps_action = utime.ticks_ms()
    temps_reaction = utime.ticks_diff(temps_action, temps_0)
    print("Votre temps de reaction est de : " + str(temps_reaction) + " milliseconds !")

led.value(1)
utime.sleep(urandom.uniform(5, 10))
led.value(0)

temps_0 = utime.ticks_ms() 
bouton.irq(trigger=machine.Pin.IRQ_RISING, handler=action_bouton)