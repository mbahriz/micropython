# CODE : diode_RGB1.py
'''
cas anode commune la plus grande
patte reliee a la masse.
'''
import machine
import utime

# convertion u16 -> pourcentage
convertion = 65535/100

# configuration des GPIOs
rouge = machine.PWM(machine.Pin(13))
bleu = machine.PWM(machine.Pin(14))
vert = machine.PWM(machine.Pin(15))

# fixe la frequence des PWM
frequence = 1000 # en Hz
rouge.freq(frequence)
bleu.freq(frequence)
vert.freq(frequence)

# Augmente le rapport cyclique du rouge de 0 a 100%
for rapport_cyclique in range (0,100):
    print('rapport cyclique = ',rapport_cyclique,' %')
    rouge.duty_u16(int(rapport_cyclique*convertion))
    bleu.duty_u16(0)
    vert.duty_u16(0)
    utime.sleep(0.1)

