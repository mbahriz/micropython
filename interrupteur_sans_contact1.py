# CODE : interrupteur_sans_contact1.py
from machine import *
import utime

# les constantes
conversion = 3.3/65535
seuil = ??? # A DETERMINER

# configuration GPIO
PIN_lecture = machine.ADC(28)
LED = Pin(15, Pin.OUT)
LED.value(0)

while True:
    tension = PIN_lecture.read_u16()*conversion
    time = utime.ticks_us()
    if tension < 4:
        print("U = {:.6f} V Useuil = {:.6f} V".format(tension,seuil))
    if tension < seuil:
        LED.value(1)
    else:
        LED.value(0)
    utime.sleep(0.05)
