# CODE : touch_pad.py
from machine import *
from utime import *

led= Pin(25, Pin.OUT)
led.value(0)

send= Pin(15, Pin.OUT) # broche connectee a une resistance de  ???ohms
send.value(0)

t1= Pin(14,Pin.IN) # Broche connectee directement au pave tactile
sleep(1)

trigLevel= 0

def getT1(): # renvoie le temps mis par le condensateur pour atteindre le niveau 1
    start=0
    end=0
    start= ticks_us()
    send.value(1)
    while t1.value()<1:
        pass
    end= ticks_us()
    send.value(0)
    return(end-start)

def calibT1():
    global trigLevel
    for i in range(0,50):
        factor = 1.3 # facteur a ajuster pour fixer le seuil de detection
        trigLevel= max(getT1()*factor , trigLevel) 
        print(".",end="")
        sleep(0.05)

print("Calibratiion....",end="")
calibT1()
print("TrigLevel: ", trigLevel)

actT1= False

while True:
    lastT1= actT1
    t1Val= getT1()
    
    actT1= t1Val > trigLevel
    if actT1 and lastT1: # allume la LED uniquement si on a deux positifs de suite
        led.value(1)
    elif actT1==False and lastT1==False:
        led.value(0)
    print(trigLevel,t1Val) # utile pour le plotter de l'IDE Thonny
    sleep(0.05)