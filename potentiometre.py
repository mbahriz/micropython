# CODE : potentiometre.py
import machine
import utime

# les constantes
conversion = 3.3/65535

# configuration GPIO
potentiometre = machine.ADC(28)

while True:
    potentiel = potentiometre.read_u16()*conversion
    print("V = {:.2f} V".format(potentiel))
    utime.sleep(0.05)