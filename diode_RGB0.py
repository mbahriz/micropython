# CODE : diode_RGB0.py
'''
cas anonde commune la plus grande
patte reliee a la masse.
'''
import machine

# configuration des GPIOs
rouge = machine.Pin(13, machine.Pin.OUT)
bleu = machine.Pin(14, machine.Pin.OUT)
vert = machine.Pin(15, machine.Pin.OUT)

# Allume le rouge
rouge.value(1)
bleu.value(0)
vert.value(0)

