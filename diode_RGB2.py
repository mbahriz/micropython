# CODE : diode_RGB2.py
'''
cas anode commune la plus grande
patte reliee a la masse.
'''
import machine
import utime

# les constantes
frequence = 1000 # en Hz
seuil1 = 1
seuil2 = 2

# configuration des GPIOs
rouge = machine.PWM(machine.Pin(13))
bleu = machine.PWM(machine.Pin(14))
vert = machine.PWM(machine.Pin(15))
potentiometre = machine.ADC(28)

# fixe la frequence des PWM
rouge.freq(frequence)
bleu.freq(frequence)
vert.freq(frequence)

# les fonctions couleurs 
def affiche_du_orange():
    rouge.duty_u16(int(100*65535/100))
    bleu.duty_u16(int(0*65535/100))
    vert.duty_u16(int(20*65535/100))
def affiche_du_rouge():
    rouge.duty_u16(int(100*65535/100))
    bleu.duty_u16(int(0*65535/100))
    vert.duty_u16(int(0*65535/100))
def affiche_du_bleu():
    rouge.duty_u16(int(0*65535/100))
    bleu.duty_u16(int(100*65535/100))
    vert.duty_u16(int(0*65535/100))
    
# boucle mesurant le potentiel du potentiometre
while True:
    potentiel = potentiometre.read_u16()*3.3/65535
    print("V = {:.2f} V".format(potentiel))
    utime.sleep(0.05)
    # test pour l affichage des couleurs
    if potentiel <= seuil1:
        affiche_du_bleu()
    elif potentiel >= seuil2:
        affiche_du_rouge()
    else:
        affiche_du_orange()