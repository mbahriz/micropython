# CODE : interrupteur_sans_contact0.py
from machine import *
import utime

# les constantes
conversion = 3.3/65535
seuil = ??? # A DETERMINER

# configuration GPIO
PIN_lecture = machine.ADC(28)

while True:
    tension = PIN_lecture.read_u16()*conversion
    time = utime.ticks_us()
    if tension < 4:
        print("U = {:.6f} V Useuil = {:.6f} V".format(tension,seuil))
    utime.sleep(0.05)
