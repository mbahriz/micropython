# CODE : capa1.py
import machine
import utime

# les constantes
temps_decharge = ??? # en seconde
tension_charge = ??? # en volt
conversion = 3.3/65535
R = 47000 # valeur de la resistance en ohms

# declaration des broches
PIN_gene = machine.Pin(15, machine.Pin.OUT)
PIN_lire = machine.ADC(28)

# decharge du condensateur
PIN_gene.value(0) # fixe le generateur a 0V
utime.sleep(temps_decharge)

# charge du condensateur
PIN_gene.value(1) # fixe le generateur a 3,3V
t_debut = utime.ticks_us()
while True:
    tension = PIN_lire.read_u16()*conversion
    print("Uc = {:.3f} V".format(tension))
    if tension >= tension_charge:
        t_fin = utime.ticks_us()
        tau = t_fin-t_debut
        print("tau = {:.3f} ms".format(tau*1e-3))
        print("C = {:.3f} uF".format(tau*1e-6/R*1e6))
        break 