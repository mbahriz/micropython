import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import FuncFormatter, MultipleLocator

#R = 1
C = 100e-9 
L = 27e-6

f0 = 1/(2*np.pi*np.sqrt(L*C))
omega0 = 2*np.pi*f0
#Q = L*omega0/R

####################################################
#---- TRANSFERT FUNCTION
####################################################

def H(f,R):
    omega = 2*np.pi*f
    return R/(R+1j*(L*omega-1/C/omega))

FREQUENCE = np.geomspace(1e4,1e6,1e5)
MODULE1 = np.abs(H(FREQUENCE,R=1))
PHASE1 = np.angle(H(FREQUENCE,R=1))
MODULE2 = np.abs(H(FREQUENCE,R=10))
PHASE2 = np.angle(H(FREQUENCE,R=10))
MODULE3 = np.abs(H(FREQUENCE,R=100))
PHASE3 = np.angle(H(FREQUENCE,R=100))
####################################################
#---- GRAPH 
####################################################

####################################################
#---- GRAPH PARAMETERS
####################################################
def load_mpl_params():
    '''
    define all parameters for the graph
    '''
    # MATPLOTLIB PARAMETERS
    mpl.rcParams.update(mpl.rcParamsDefault)
    # mpl.rcParamsDefault.keys --> display all parameters available
    params = {'legend.fontsize': 16,  
              'figure.figsize': (10, 8), #(10, 8)
              'axes.labelsize': 18,
              'axes.titlesize': 18,
              'xtick.labelsize': 18,
              'ytick.labelsize': 18,
              'figure.subplot.left': 0.15, #0.15
              'figure.subplot.right': 0.85, #0.85
              'figure.subplot.bottom': 0.15,
              'figure.subplot.top': 0.85,
              'xtick.direction': 'in',
              'ytick.direction': 'in',
              'xtick.major.size': 5,
              'xtick.major.width': 1.3,
              'xtick.minor.size': 3,
              'xtick.minor.width': 1,
              'xtick.major.pad': 8,
              'ytick.major.pad': 8,
              'lines.linewidth': 1,
              'axes.grid': 'True',
              'axes.grid.axis': 'both',
              'axes.grid.which': 'both',
              'grid.alpha': 0.5,
              'grid.color': '111111',
              'grid.linestyle': '--',
              'grid.linewidth': 0.8,
              'savefig.dpi': 300, }
    mpl.rcParams.update(params)

load_mpl_params()

### List of color
number_color = 20
cmap = plt.get_cmap('tab20c')
colors = [cmap(i) for i in np.linspace(0, 1, number_color)]


##### graph A
gridsize = (1, 1)
fig, _ = plt.subplots()

ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
ax1.plot(FREQUENCE*1e-3,MODULE1,color=colors[0],linewidth=3,label=r'module R=1$\Omega$')
ax1.plot(FREQUENCE*1e-3,MODULE2,color=colors[1],linewidth=1,label=r'module R=10$\Omega$')
ax1.plot(FREQUENCE*1e-3,MODULE3,color=colors[2],linewidth=1,label=r'module R=100$\Omega$')
ax1.set_xlabel('fréquence (kHz)', labelpad=0)
ax1.set_ylabel(r' {}T$_R$($\omega$){}'.format(chr(124),chr(124)), labelpad=4, color=colors[0])
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.tick_params(axis='y', labelcolor=colors[0])
ax1.set_xlim([1e1,1e3])
ax1.set_ylim([1e-3,2])

ax1.legend(loc=3)

# ax1.set_yticks([1e-3,1e-2,1e-1,1/np.sqrt(2),1])
# labels = ['0.001','0.01','0.1',r'$\frac{1}{\sqrt{2}}$','1']
# ax1.set_yticklabels(labels)

ax2 = ax1.twinx()
ax2.plot(FREQUENCE*1e-3,PHASE1,color=colors[4],linewidth=3, label=r'phase R=1$\Omega$')
ax2.plot(FREQUENCE*1e-3,PHASE2,color=colors[5],linewidth=1, label=r'phase R=10$\Omega$')
ax2.plot(FREQUENCE*1e-3,PHASE3,color=colors[6],linewidth=1, label=r'phase R=100$\Omega$')
ax2.set_ylabel(r'arg(T$_R$($\omega$))',labelpad=4, color=colors[4])
ax2.legend(loc=1)
ax2.tick_params(axis='y', labelcolor=colors[4])
ax2.grid(b=False)
ax2.set_yticks([np.pi/2,0,-np.pi/2])
labels = [r'$\pi/2$', '0', r'-$\pi/2$']
ax2.set_yticklabels(labels)
ax2.set_ylim([-np.pi/2,np.pi/2])

# # frequence de coupure
ax1.scatter(f0*1e-3, 1, s=75, color=colors[0], alpha=0.50,edgecolors=colors[1],zorder=3)
ax1.plot([0,f0*1e-3],[1,1],color=colors[1],linewidth=1.5, dashes=[4,2])
ax1.plot([f0*1e-3,f0*1e-3],[1e-3,1],color=colors[1],linewidth=1.5, dashes=[4,2])
ax1.text((f0+7000)*1e-3, 1.1, r'f$_0$', rotation=0,color=colors[0],fontsize=18)
ax2.scatter(f0*1e-3, 0, s=75, color=colors[4], alpha=0.50,edgecolors=colors[5],zorder=3)
ax2.plot([f0*1e-3,1e8],[0,0],color=colors[5],linewidth=1.5, dashes=[3,2])
ax2.plot([f0*1e-3,f0*1e-3],[-np.pi/2,0],color=colors[5],linewidth=1.5, dashes=[3,2])
ax2.text((f0+7000)*1e-3, 0.05, r'f$_0$', rotation=0,color=colors[5],fontsize=18)

# plt.title(r'R={:.0f}$\Omega$ L={:.0f}$\mu$H C={:.0f}nF f$_0$={:.2f}kHz Q={:.3f}'.format(R,L*1e6,C*1e9,f0*1e-3,Q))

fig.savefig('resonnance.pdf')
plt.show()





