import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import FuncFormatter, MultipleLocator

E = 10
R = 500
C = 1e-3
tau = R*C 

fc = 1/R/C/2/np.pi

####################################################
#---- TRANSFERT FUNCTION
####################################################

def charge(t):
    u = E*(1-np.exp(-t/tau))
    return u

temps = np.linspace(0,25,1e4)


####################################################
#---- GRAPH 
####################################################

####################################################
#---- GRAPH PARAMETERS
####################################################
def load_mpl_params():
    '''
    define all parameters for the graph
    '''
    # MATPLOTLIB PARAMETERS
    mpl.rcParams.update(mpl.rcParamsDefault)
    # mpl.rcParamsDefault.keys --> display all parameters available
    params = {'legend.fontsize': 16,  
              'figure.figsize': (10, 8), #(10, 8)
              'axes.labelsize': 18,
              'axes.titlesize': 18,
              'xtick.labelsize': 18,
              'ytick.labelsize': 18,
              'figure.subplot.left': 0.15, #0.15
              'figure.subplot.right': 0.85, #0.85
              'figure.subplot.bottom': 0.15,
              'figure.subplot.top': 0.85,
              'xtick.direction': 'in',
              'ytick.direction': 'in',
              'xtick.major.size': 5,
              'xtick.major.width': 1.3,
              'xtick.minor.size': 3,
              'xtick.minor.width': 1,
              'xtick.major.pad': 8,
              'ytick.major.pad': 8,
              'lines.linewidth': 1,
              'axes.grid': 'True',
              'axes.grid.axis': 'both',
              'axes.grid.which': 'both',
              'grid.alpha': 0.5,
              'grid.color': '111111',
              'grid.linestyle': '--',
              'grid.linewidth': 0.8,
              'savefig.dpi': 300, }
    mpl.rcParams.update(params)

load_mpl_params()

### List of color
number_color = 10
cmap = plt.get_cmap('tab10')
colors = [cmap(i) for i in np.linspace(0, 1, number_color)]


##### graph A
gridsize = (1, 1)
fig, _ = plt.subplots()

ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
i=0
for tau in [0.5,1,2,4]:
    ax1.plot(temps,charge(temps),color=colors[i],linewidth=2,label=r'u$_C$(t) pour $\tau=RC={:g}$s'.format(tau))
    i+=1
ax1.plot([-5,0,0,50],[0,0,E,E],color='k',linewidth=1,label=r'E(t)', dashes=[3,2])
ax1.set_xlabel('temps (s)', labelpad=0)
ax1.set_ylabel(r'u$_C$(t) (V)', labelpad=4)
ax1.set_xlim([-5,15])
ax1.set_ylim([-0.5,E+0.5])
ax1.legend(loc=0)

# ax1.set_yticks([1e-3,1e-2,1e-1,1/np.sqrt(2),1])
# labels = ['0.001','0.01','0.1',r'$\frac{1}{\sqrt{2}}$','1']
# ax1.set_yticklabels(labels)

# ax2 = ax1.twinx()
# ax2.plot(FREQUENCE,PHASE,color=colors[4],linewidth=2, label='phase')
# ax2.set_ylabel(r'arg(H($\omega$))',labelpad=4, color=colors[4])
# ax2.legend(loc=1)
# ax2.tick_params(axis='y', labelcolor=colors[4])
# ax2.grid(b=False)
# ax2.set_yticks([-np.pi/2,-np.pi/4,0])
# labels = [r'$-\pi/2$', r'$-\pi/4$', '$0$']
# ax2.set_yticklabels(labels)

# # frequence de coupure
# ax1.scatter(fc, 1/np.sqrt(2), s=75, color=colors[0], alpha=0.50,edgecolors=colors[1],zorder=3)
# ax1.plot([0,fc],[1/np.sqrt(2),1/np.sqrt(2)],color=colors[1],linewidth=1.5, dashes=[4,2])
# ax1.plot([fc,fc],[1e-3,1/np.sqrt(2)],color=colors[1],linewidth=1.5, dashes=[4,2])
# ax2.scatter(fc, -np.pi/4, s=75, color=colors[4], alpha=0.50,edgecolors=colors[5],zorder=3)
# ax2.plot([fc,1e5],[-np.pi/4,-np.pi/4],color=colors[5],linewidth=1.5, dashes=[3,2])
# ax2.plot([fc,fc],[-np.pi/2,-np.pi/4],color=colors[5],linewidth=1.5, dashes=[3,2])
# ax1.text(fc, 0.7e-3, r'f$_c$', rotation=0,color=colors[8],fontsize=18)

plt.title(r'Charge du condensatuer avec E={:.0f}V'.format(E))

fig.savefig('charge.pdf')
plt.show()





