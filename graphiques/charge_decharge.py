import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.ticker import FuncFormatter, MultipleLocator

E = 3.3
R = 47e3
C = 47e-6
tau = R*C 

fc = 1/R/C/2/np.pi

####################################################
#---- TRANSFERT FUNCTION
####################################################

def charge(t):
    u = E*(1-np.exp(-t/tau))
    return u

def decharge(t):
    u = E*np.exp(-t/tau)
    return u

periode = 10*tau
temps = np.linspace(0,int(periode),1e4)
print(temps)
####################################################
#---- GRAPH 
####################################################

####################################################
#---- GRAPH PARAMETERS
####################################################
def load_mpl_params():
    '''
    define all parameters for the graph
    '''
    # MATPLOTLIB PARAMETERS
    mpl.rcParams.update(mpl.rcParamsDefault)
    # mpl.rcParamsDefault.keys --> display all parameters available
    params = {'legend.fontsize': 16,  
              'figure.figsize': (10, 8), #(10, 8)
              'axes.labelsize': 18,
              'axes.titlesize': 18,
              'xtick.labelsize': 18,
              'ytick.labelsize': 18,
              'figure.subplot.left': 0.15, #0.15
              'figure.subplot.right': 0.85, #0.85
              'figure.subplot.bottom': 0.15,
              'figure.subplot.top': 0.85,
              'xtick.direction': 'in',
              'ytick.direction': 'in',
              'xtick.major.size': 5,
              'xtick.major.width': 1.3,
              'xtick.minor.size': 3,
              'xtick.minor.width': 1,
              'xtick.major.pad': 8,
              'ytick.major.pad': 8,
              'lines.linewidth': 1,
              'axes.grid': 'True',
              'axes.grid.axis': 'both',
              'axes.grid.which': 'both',
              'grid.alpha': 0.5,
              'grid.color': '111111',
              'grid.linestyle': '--',
              'grid.linewidth': 0.8,
              'savefig.dpi': 300, }
    mpl.rcParams.update(params)

load_mpl_params()

### List of color
number_color = 20
cmap = plt.get_cmap('tab20b')
colors = [cmap(i) for i in np.linspace(0, 1, number_color)]


##### graph A
gridsize = (1, 1)
fig, _ = plt.subplots()

ax1 = plt.subplot2grid(gridsize, (0, 0), colspan=1, rowspan=1)
ax1.plot([0,periode,periode,2*periode],[0,0,E,E],color='k',linewidth=2,label=r'E(t)', dashes=[3,2])
ax1.plot(temps,decharge(temps),color=colors[1],linewidth=2,label=r'décharge')
ax1.plot(temps+periode,charge(temps),color=colors[17],linewidth=2,label=r'charge')
ax1.set_xlabel('temps (s)', labelpad=0)
ax1.set_ylabel(r'u$_C$(t) (V)', labelpad=4)
ax1.set_xlim([0,2*periode-8])
y_max = E+0.5
y_min = -0.1
ax1.set_ylim([y_min,y_max])
ax1.legend(loc=0)

# temps de décharge
temps_decharge = -1*tau*np.log(0.001)
print("temps de décharge = {:.3f} s".format(temps_decharge))
ax1.scatter(temps_decharge, 0.001*E, s=75, color=colors[3], alpha=0.50,edgecolors=colors[0],zorder=3)
ax1.text(10, 0.09, r'temps_décharge', rotation=0,color=colors[0],fontsize=14)
#ax1.plot([temps_decharge,temps_decharge],[y_min,0.001*E],color=colors[0],linewidth=2, dashes=[1,1])

# tension charge
tension_charge = E*(1-np.exp(-1))
print("tension de charge = {:.3f} V".format(tension_charge))
ax1.scatter(tau+periode, tension_charge, s=75, color=colors[18], alpha=0.50,edgecolors=colors[16],zorder=3)
ax1.text(tau+periode+1, tension_charge, r'tension_charge', rotation=0,color=colors[16],fontsize=14)
#ax1.plot([0,tau+periode],[tension_charge,tension_charge],color=colors[16],linewidth=2, dashes=[1,1])

plt.title(r'Décharge et charge (R={:.0f}k$\Omega$ C={:.0f}$\mu$F E={:.1f}V)'.format(R/1000,C*1e6,E))

fig.savefig('charge_decharge.pdf')
plt.show()





