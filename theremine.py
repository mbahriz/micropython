# CODE : theremine.py
from machine import *
import utime

# les constantes
freq_max = 1000
freq_min = 10

# configuration GPIO
PIN_lecture = machine.ADC(28)
BUZZER = PWM(Pin(0))
BUZZER.freq(100)
BUZZER.duty_u16(0)

while True:
    frequence = PIN_lecture.read_u16()*(freq_max-freq_min)/65535+freq_min
    time = utime.ticks_us()
    print("frequence = {:.0f} Hz".format(frequence))
    BUZZER.freq(int(frequence))
    BUZZER.duty_u16(1000)
    utime.sleep(0.05)
