# CODE : buzzer0.py
import machine 
import utime

buzzer = machine.PWM(machine.Pin(0))
buzzer.freq(250)
buzzer.duty_u16(10000)
utime.sleep(0.5)
buzzer.duty_u16(0)