# CODE : reaction_game0.py
import machine
import utime
import urandom

led = machine.Pin(15, machine.Pin.OUT)
bouton = machine.Pin(16, machine.Pin.IN, machine.Pin.PULL_DOWN)

def action_bouton(pin):
    bouton.irq(handler=None)
    print(pin)

led.value(1)
# la fonction urandom.uniform(5,10) fournit un nombre aleatoire compris enter 5 et 10
utime.sleep(urandom.uniform(5,10))
led.value(0)
bouton.irq(trigger=machine.Pin.IRQ_RISING, handler=action_bouton)