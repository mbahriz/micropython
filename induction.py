# CODE : induction.py
import machine
import utime

# les constantes
conversion = 3.3/65535
f0 = 68e3 #frequence de resonance

# configuration GPIO
PIN_lecture = machine.ADC(28)
PIN_generateur = machine.PWM(machine.Pin(0))

# configuration du generateur
PIN_generateur.freq(int(f0))
PIN_generateur.duty_u16(32512)
utime.sleep(0.1)

while True:
    tension = PIN_lecture.read_u16()*conversion
    print(tension)
    utime.sleep(0.1)