# CODE : test.py
import machine
import utime

# les constantes
conversion = 3.3/65535
f0 = 68e3 #frequence de resonance

# configuration GPIO
PIN_lecture = machine.ADC(28)
PIN_generateur = machine.PWM(machine.Pin(0))

# configuration du generateur
PIN_generateur.freq(int(2053 e3))
PIN_generateur.duty_u16(32512)
utime.sleep(0.1)

while True:
    tension = PIN_lecture.read_u16()*conversion
    print(tension)
    utime.sleep(0.01)
    

# # les constantes
# conversion = 3.3/65535

# # configuration GPIO
# PIN_lecture = machine.ADC(28)
# PIN_generateur = machine.PWM(machine.Pin(0))

# # coniguration du generateur
# PIN_generateur.freq(68000)
# PIN_generateur.duty_u16(32512)
# utime.sleep(1)

# def max_tension(frequence):
#     laps = 100/frequence*1e3
#     t_maintenant = utime.ticks_us()
#     t_fin = utime.ticks_us()+laps
#     tension_max = 0
#     while t_maintenant < t_fin:
#         t_maintenant = utime.ticks_us()
#         tension = PIN_lecture.read_u16()*conversion
#         utime.sleep(0.005)
#         if tension > tension_max:
#             tension_max = tension
#             print(freq,tension)         
#     return tension_max
            
# for freq in range(50e3,1000e3,1000):
#     tension = max_tension(freq)
#     #print("f={:.g}kHz Umax={:.6f}V".format(freq/1e3,tension))
#     utime.sleep(0.05)
