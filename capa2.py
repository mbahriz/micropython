# CODE : capa2.py
import machine
import utime

# les constantes
temps_decharge = ??? # en seconde # A DETERMINER
tension_charge = ??? # en volt # A DETERMINER
conversion = 3.3/65535
R = 47000 # valeur de la resistance en ohms

# broche fixe
PIN_lecture = machine.ADC(28)

# decharge du condensateur
PIN_charge = machine.Pin(15, machine.Pin.IN)
PIN_decharge = machine.Pin(14, machine.Pin.OUT)
PIN_decharge.value(0) # fixe le generateur a 0V
utime.sleep(temps_decharge)

# charge du condensateur
PIN_charge = machine.Pin(15, machine.Pin.OUT)
PIN_decharge = machine.Pin(14, machine.Pin.IN)
PIN_charge.value(1) # fixe le generateur a 3,3V
t_debut = utime.ticks_us()
while True:
    tension = PIN_lecture.read_u16()*conversion
    print("Uc = {:.3f} V".format(tension))
    if tension >= tension_charge:
        t_fin = utime.ticks_us()
        tau = t_fin-t_debut
        print("tau = {:.3f} ms".format(tau*1e-3))
        print("C = {:.3f} uF".format(tau*1e-6/R*1e6))
        break 