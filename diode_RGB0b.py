# CODE : diode_RGB0b.py
'''
cas cathode commune la plus grande
patte reliee aux 3,3 V.
'''
import machine

# configuration des GPIOs
rouge = machine.Pin(13, machine.Pin.OUT)
bleu = machine.Pin(14, machine.Pin.OUT)
vert = machine.Pin(15, machine.Pin.OUT)

# Allume le rouge
rouge.value(0)
bleu.value(1)
vert.value(1)

