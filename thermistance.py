# CODE : thermistance.py
import machine
import utime
import math

# les constantes
conversion = 3.3/65535

# configuration GPIO
potentiometre = machine.ADC(28)

# calcul la temperature
def temperature(R):
    # valeurs du constructeur
    R_ref = 1000
    A = 3.354016E-03
    B = 2.933908E-04
    C = 3.494314E-06
    D = -7.712690E-07
    # equation
    part1 = A + B*math.log(R/R_ref)
    part2 = C*(math.log(R/R_ref))**2+D*(math.log(R/R_ref))**3
    T = 1/(part1+part2)
    # conversion des Kelvins en degres
    T = T - 273.15
    return T

while True:
    potentiel = potentiometre.read_u16()*conversion
    R = 1000*(3.3/potentiel-1)
    T = temperature(R)
    print("Temp. = {:.3f} Celcius".format(T))
    utime.sleep(0.1)